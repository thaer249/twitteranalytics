;
(function() {

    var $tweetsContainer = jQuery('#tweets ul');

    var map = L.map('map');

    L.tileLayer('http://{s}.tile.cloudmade.com/42474b5ca238448e8eed10814469aee6/88251/256/{z}/{x}/{y}.png', {
            attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>',
            maxZoom: 18
        }).addTo(map);

    map.setView([16.636192, 42.363281], 3);

    /*
     * socket.io
     */

    var socket = io.connect();
    socket.on('tweet', function(data) {

        // console.log(data);

        // var marker = L.marker([data.coordinates[1], data.coordinates[0]]).addTo(map);

        var circle = L.circle([data.coordinates[1], data.coordinates[0]], 2, {
            color: 'red',
            fillColor: '#f03',
            fillOpacity: 0.5
        }).addTo(map);

        var userLink = "<a target=_blank href=http://twitter.com/" + data.screen_name + " >" + "@" + data.screen_name + "</a>";

        circle.bindPopup("<b><img src=" + data.pic + " alt='' />" + userLink + "</b><br>" + data.text );

        circle.on('click', function(){
            this.openPopup();
        });

        setInterval(function(){
            map.removeLayer(circle);
        }, 30 * 1000);

        // Attach info window to marker
        
        // marker.bindPopup("<b><img src=" + data.pic + " alt='' />@" + data.screen_name + "</b><br>" + data.text ).openPopup();

        // marker.on('mouseover', function(){
        //     this.openPopup();
        // });

        // marker.on('mouseout', function(){
        //     this.closePopup();
        // });

    });

})();