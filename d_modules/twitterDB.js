var db_conf = require('../db_conf.json'),
    mysql_db = require('mysql').createConnection(db_conf);

mysql_db.connect(function(err){
    if (err) {
        console.log('Failed to connect to DB');
        return;
    }

    console.log('Connected to DB!!');
});

exports.saveTweet = function (tweet, cleanText) {
	mysql_db.query("INSERT INTO tweet (text, cleanText, screen_name) VALUES ('" + mysql_db.escape(tweet.text) + "', '" + mysql_db.escape(cleanText) + "', '" + mysql_db.escape(tweet.user.screen_name) + "')");
};